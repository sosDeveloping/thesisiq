from finta import TA
import pandas as pd
import numpy as np


def StrategyIndicators(df):

    indicator = df.rename(columns = {'min':'low', 'max':'high'}) # raname DF columns
   
   

    # simple moving avarage
    df['SMA_20'] = TA.SSMA(indicator,20)
    df['SMA_50'] = TA.SSMA(indicator,50)
    
    # exponential moving average
    df['EMA_20'] = TA.EMA(indicator,20, adjust=False)
    df['EMA_50'] = TA.EMA(indicator,50, adjust=False)
    

     #Stochastic Oscillator
    df['%K'] = TA.STOCH(indicator, 14)
    df['%D'] = TA.STOCHD(indicator, 14)

    
    df['rsi'] = TA.RSI(indicator, 14)
    
  


    
    df['VWAP'] = TA.VWAP(indicator)
    

    
    df = df.rename(columns = {'low':'min', 'high':'max'})

    
    return df
