import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from collections import deque
from iq import login, lower, fast_data, get_balance, get_payout, higher
from indicator import StrategyIndicators
from training import train_data
import datetime
import time
import tensorflow as tf
import sys
import os


# Just disables the warning, doesn't enable AVX/FMA
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# Suppressing deprecated warnings
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)



iq = login(email = 'USER', password='PASSWORD', AccountType='DEMO') # REAL OR DEMO
ratio = 'EURUSD'        # SIMBOLO ATIVO
bet_money = 100           # VALOR INICIAL DO TRADE
MAX_GALE = 15           # QUANTIDADE MAXIMA DE MARTINGALE
MIN_PAYOUT = 0.60       # MINIMO PAYOUT PARA TRADE
MIN_BALANCE = 0         # VALOR MINIMO NA CONTA PARA ABRIR UM TRADE
MIN_SIDE = 0.51         # PORCENTAGEM MINIMA PARA ABIR UM TRADE EXEMPLO 50 QUALQUER LADO COM 51 SEGUE ESTE


############ NÃO ALTERAR CODIGO ABAIXO #####################




INICIAL_GALE = 2
MIN_ENTRY = bet_money

Wtrade = 0 #  N° of Buyers Trades
Ltrade = 0 #  N° of Sellers  Trades
NtradeS = 0
NtradeB = 0
Tiedtrade = 0
def preprocess_prediciton(iq,ratio):
    Actives = ['EURUSD']
    active = ratio
    main = pd.DataFrame()
    current = pd.DataFrame()
    for active in Actives:
        if active == ratio:
            main = fast_data(iq,active).drop(columns = {'from','to'})
        else:
            current = fast_data(iq,active)
            current = current.drop(columns = {'from','to','open','min','max'})
            current.columns = [f'close_{active}',f'volume_{active}']
            main = main.join(current)

    df = main

    """
    graphical analysis components
    """

    df.isnull().sum().sum() 
    df.fillna(method="ffill", inplace=True)
    df = df.loc[~df.index.duplicated(keep = 'first')]
    

    df = StrategyIndicators(df)



    #df = df.drop(columns = {'open','min','max'})

    df = df.dropna()
    df = df.fillna(method="ffill")
    df = df.dropna()

    df.sort_index(inplace = True)

    scaler = MinMaxScaler()
    indexes = df.index
    df_scaled = scaler.fit_transform(df)

    pred = pd.DataFrame(df_scaled,index = indexes)

    sequential_data = []
    prev_days = deque(maxlen = SEQ_LEN)

    for i in pred.iloc[len(pred) -SEQ_LEN :len(pred)   , :].values:
        prev_days.append([n for n in i[:]])
        if len(prev_days) == SEQ_LEN:
            sequential_data.append([np.array(prev_days)])

    X = []

    for seq in sequential_data:
        X.append(seq)
        
        


    return np.array(X)

SEQ_LEN = 5 
FUTURE_PERIOD_PREDICT = 2 


NAME = train_data(iq,ratio) + '.h5'
model = tf.keras.models.load_model(f'models/{NAME}')

# define the countdown func. 
def countdown(t): 
    
    while t: 
        mins, secs = divmod(t, 60) 
        timer = '{:02d}:{:02d}'.format(mins, secs) 
        print(timer, end="\r") 
        time.sleep(1) 
        t -= 1
      
    print('Ready for the Nex Trade!!')
     
def percentage(entry1, entry2):
    try:
        return ( 100 * entry1 /entry2) 
    except ZeroDivisionError:
        return 0


i = 0
l = 0 
bid = True
bets = []

trade = True


while(1):
    t = 60
    if i >= 10 and i % 2 == 0:
        NAME = train_data(iq,ratio) + '.h5'
        model = tf.keras.models.load_model(f'models/{NAME}')
        i = 0
    if datetime.datetime.now().second < 30 and i % 2 == 0: #GARANTE QUE ELE VAI APOSTAR NA SEGUNDA, POIS AQUI ELE JÁ PEGA OS DADOS DE UMA NA FRENTE,
        time_taker = time.time()
        pred_ready = preprocess_prediciton(iq,ratio)             
        pred_ready = pred_ready.reshape(1,SEQ_LEN,pred_ready.shape[3])     
        result = model.predict(pred_ready)
       
        print("probability of PUT: {:.2f}%".format(round(result[0][0],2)))
        print("probability of CALL: {:.2f}%".format(round(result[0][1],2)))
        print(f'Time taken : {int(time.time()-time_taker)} seconds')
        i = i + 1
        payout = get_payout(iq,ratio)
        balance = get_balance(iq)
        print(f'Simbol : {ratio}')
        print(f'Balance : {balance}')
        print("Payout: {:.2f}%".format(payout))
        print("BET: {:.2f}$".format(bet_money))
        print("Next Martingale: {:.2f}$".format(bet_money * round(INICIAL_GALE/get_payout(iq,ratio),2)))
        print ("Winning Rate : {:.2f}%".format(percentage(Wtrade,NtradeB+NtradeS))+'\n'+"Trade N°: "+str(NtradeS+NtradeB)+'\n')
        print ("Winning Rate Tied Calculation : {:.2f}%".format(percentage(Wtrade,NtradeB+NtradeS-Tiedtrade))+'\n'+"Trade N°: "+str(NtradeS+NtradeB)+'\n')
       
       

    if datetime.datetime.now().second == 59 and i%2 == 1:
        if result[0][0] > result[0][1] and result[0][0] > MIN_SIDE and get_payout(iq,ratio) >=MIN_PAYOUT and balance > MIN_BALANCE:
            print("PUT")
            id = lower(iq,bet_money,ratio)
            NtradeS += 1
            i = i + 1
            
            trade = True
        elif result[0][1] > result[0][0] and result[0][1] > MIN_SIDE and get_payout(iq,ratio) >=MIN_PAYOUT and balance > MIN_BALANCE:
            print("CALL")
            id = higher(iq,bet_money,ratio) 
            NtradeB += 1
            
            i = i + 1
            
            trade = True
        else:
            trade = False
            i = i + 1
        if trade:
            countdown(60)
            time.sleep(6)            
            
            betsies = iq.get_optioninfo_v2(1)
            betsies = betsies['msg']['closed_options']
            for bt in betsies:
                bets.append(bt['win'])
            win = bets[-1:]
            if win == ['win']:
                print("Win")
                l = 0
                Wtrade += 1
                #print(f'Balance : {get_balance(iq)}')
                bet_money = MIN_ENTRY
            elif win == ['loose']:
                print("Loss")
                
                l = l + 1
                if l >= MAX_GALE:
                    l = 0
                    i = 10
                    bet_money = MIN_ENTRY
                   
                else:
                    bet_money = bet_money * round(INICIAL_GALE/get_payout(iq,ratio),2)
            elif win == ['equal']:
                print('Tied Wait for 10 minutes befor next Trade')
                Tiedtrade += 1
                countdown(600)
                i=10
                  
            else:
                #print(f'Balance : {get_balance(iq)}')
                bets.append(0)
            #print(bet_money)
