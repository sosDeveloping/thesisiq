from finta import TA
import pandas as pd
import numpy as np
from candlestick import candlestick

def StrategyIndicators(df):
    indicator = df.rename(columns = {'min':'low', 'max':'high'}) # raname DF columns


    df1 = candlestick.bearish_harami(indicator, target='bearish_harami')
    df1 = df1.drop(columns = {'volume','open','high','low','close'})
    df = df1.join(df)
    df["bearish_harami"] = df["bearish_harami"].astype(bool)
    


    df1 = candlestick.bullish_harami(indicator, target='bullish_harami')
    df1 = df1.drop(columns = {'volume','open','high','low','close'})
    df = df1.join(df)
    df["bullish_harami"] = df["bullish_harami"].astype(bool)
    df["bullish_harami"] = df["bullish_harami"].astype(int)


    # df1 = candlestick.doji(indicator, target='doji')
    # df1 = df1.drop(columns = {'volume','open','high','low','close'})
    # df = df1.join(df)
    # df["doji"] = df["doji"].astype(bool)
    # df["doji"] = df["doji"].astype(int)



    # df1 = candlestick.dark_cloud_cover(indicator, target='dark_cloud_cover')
    # df1 = df1.drop(columns = {'volume','open','high','low','close'})
    # df = df1.join(df)
    # df["dark_cloud_cover"] = df["dark_cloud_cover"].astype(bool)
    # df["dark_cloud_cover"] = df["dark_cloud_cover"].astype(int)


    # df1 = candlestick.morning_star_doji(indicator, target='morning_star_doji')
    # df1 = df1.drop(columns = {'volume','open','high','low','close'})
    # df = df1.join(df)
    # df["morning_star_doji"] = df["morning_star_doji"].astype(bool)
    # df["morning_star_doji"] = df["morning_star_doji"].astype(int)


    # df1 = candlestick.shooting_star(indicator, target='shooting_star')
    # df1 = df1.drop(columns = {'volume','open','high','low','close'})
    # df = df1.join(df)
    # df["shooting_star"] = df["shooting_star"].astype(bool)
    # df["shooting_star"] = df["shooting_star"].astype(int)


    
    # df1 = candlestick.dragonfly_doji(indicator, target='dragonfly_doji')
    # df1 = df1.drop(columns = {'volume','open','high','low','close'})
    # df = df1.join(df)
    # df1 = candlestick.hanging_man(indicator, target='hanging_man')
    # df1 = df1.drop(columns = {'volume','open','high','low','close'})
    # df = df1.join(df)
    # # df1 = candlestick.gravestone_doji(indicator, target='gravestone_doji')
    # # df1 = df1.drop(columns = {'volume','open','high','low','close'})
    # # df = df1.join(df)
    # df1 = candlestick.bearish_engulfing(indicator, target='bearish_engulfing')
    # df1 = df1.drop(columns = {'volume','open','high','low','close'})
    # df = df1.join(df)
    # df1 = candlestick.bullish_engulfing(indicator, target='bullish_engulfing')
    # df1 = df1.drop(columns = {'volume','open','high','low','close'})
    # df = df1.join(df)
    # df1 = candlestick.hammer(indicator, target='hammer')
    # df1 = df1.drop(columns = {'volume','open','high','low','close'})
    # df = df1.join(df)
    # df1 = candlestick.morning_star(indicator, target='morning_star')
    # df1 = df1.drop(columns = {'volume','open','high','low','close'})
    # df = df1.join(df)
    # df1 = candlestick.morning_star_doji(indicator, target='morning_star_doji')
    # df1 = df1.drop(columns = {'volume','open','high','low','close'})
    # df = df1.join(df)
    # df1 = candlestick.piercing_pattern(indicator, target='piercing_pattern')
    # df1 = df1.drop(columns = {'volume','open','high','low','close'})
    # df = df1.join(df)
    # df1 = candlestick.rain_drop(indicator, target='rain_drop')
    # df1 = df1.drop(columns = {'volume','open','high','low','close'})
    # df = df1.join(df)
    # df1 = candlestick.rain_drop_doji(indicator, target='rain_drop_doji')
    # df1 = df1.drop(columns = {'volume','open','high','low','close'})
    # df = df1.join(df)
    # df1 = candlestick.star(indicator, target='star')
    # df1 = df1.drop(columns = {'volume','open','high','low','close'})
    # df = df1.join(df)
    # df1 = candlestick.shooting_star(indicator, target='shooting_star')
    # df1 = df1.drop(columns = {'volume','open','high','low','close'})
    # df = df1.join(df)
    #simple moving avarage
    df['SMA_20'] = TA.SSMA(indicator,20)
    df['SMA_50'] = TA.SSMA(indicator,50)

    # exponential moving average
    df['EMA_20'] = TA.EMA(indicator,20, adjust=False)
    df['EMA_50'] = TA.EMA(indicator,50, adjust=False)


     #Stochastic Oscillator
    df['%K'] = TA.STOCH(indicator, 14)
    df['%D'] = TA.STOCHD(indicator, 14)

    #Relative Strenght Index 'RSI'
    df['rsi'] = TA.RSI(indicator, 14)
    # df['cci'] = TA.CCI(indicator,period=14)



    # df['VPT'] = TA.VPT(indicator)
    # df['VWAP'] = TA.VWAP(indicator)

    
 

    df = df.rename(columns = {'low':'min', 'high':'max'})

    print(df)
    return df
